#include "MpuController.h"


MpuController::MpuController()
{
    init();
}

MpuController::~MpuController()
{
}

void MpuController::init()
{
    Wire.begin();
    Wire.beginTransmission(MPU6050_addr);
    Wire.write(0x6B);
    Wire.write(0);
    Wire.endTransmission(true);

    // for(int i = 0;i<10;i++){
    //     calculated_X[i] = 0;
    // }
}

void MpuController::registerDriverController(DriverController * dController)
{
    this->driverController = dController;
}

void MpuController::doWork()
{
    getDataFromMpu();
    calculateXYZ();
    calculateTemp();

    printToSerial();
    
    
}

void MpuController::getDataFromMpu()
{
    Wire.beginTransmission(MPU6050_addr);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU6050_addr, 14, true);
    AccX = Wire.read() << 8 | Wire.read();
    AccY = Wire.read() << 8 | Wire.read();
    AccZ = Wire.read() << 8 | Wire.read();
    Temp = Wire.read() << 8 | Wire.read();
    GyroX = Wire.read() << 8 | Wire.read();
    GyroY = Wire.read() << 8 | Wire.read();
    GyroZ = Wire.read() << 8 | Wire.read();
}

void MpuController::calculateXYZ()
{
    double xAng = map(AccX,minVal,maxVal,-90,90); 
    double yAng = map(AccY,minVal,maxVal,-90,90); 
    double zAng = map(AccZ,minVal,maxVal,-90,90);

    
    // addtoXArray(RAD_TO_DEG * (atan2(-yAng, -zAng)+PI)); 
    // calculated_X = RAD_TO_DEG * (atan2(-yAng, -zAng)+PI); 
    // calculated_Y = RAD_TO_DEG * (atan2(-xAng, -zAng)+PI); 
    calculated_Z = atan2(-yAng, -xAng)+PI;

    // if(count_X == 10) count_X = 0;
    // Model gaat uit van radialen, dus geen rad to degree
    if(calculated_Z - offset > 0) fallingDirection = true; 
    else if(calculated_Z - offset < 0) fallingDirection = false; 
}

void MpuController::calculateTemp()
{
    calculated_Temp = Temp / 340.00 + 36.53;
}
wsd
void MpuController::printToSerial()
{
    // Serial.print("AccX = ");
    // Serial.print(calculated_X);
    // Serial.print(" || AccY = ");
    // Serial.print(calculated_Y);
    // Serial.print(" || AccZ = ");
    // Serial.print(calculated_Z);
    // Serial.print(" || Offset = ");
    // Serial.println(calculated_Z - offset);

}

double MpuController::getCubeAngle()
{
    return calculated_Z - offset;
}

double MpuController::getCubeSpeed()
{
    return GyroZ;
}