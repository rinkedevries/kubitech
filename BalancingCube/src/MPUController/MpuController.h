
#ifndef __MPUCONTROLLER__H
#define __MPUCONTROLLER__H

#include <Wire.h>
#include <Arduino.h>
#include "../DriverController/DriverController.h"

class MpuController
{
public:
    MpuController(/* args */);
    ~MpuController();

    void init();
    void getDataFromMpu();
    void calculateXYZ();
    void calculateTemp();
    void printToSerial();
    void doWork();

    double getCubeAngle();
    double getCubeSpeed();

    void registerDriverController(DriverController * dController);

    bool fallingDirection = false;

private:

    DriverController * driverController;

    const int MPU6050_addr = 0x68;
    int16_t AccX, AccY, AccZ, Temp, GyroX, GyroY, GyroZ;   
    const int minVal = 265; 
    const int maxVal = 402; 
    volatile double calculated_Temp = 0;
    volatile double calculated_X = 0;
    volatile double calculated_Y = 0;
    volatile double calculated_Z = 0;

    const double offset = 5.34;
};

#endif



