#include "HalController.h"

HalController::HalController(/* args */)
{
}

HalController::~HalController()
{
}

void HalController::readHalSensors()
{
    mappedValueHalSensor1 = map(analogRead(halSensor1), 0, 1023, 0, 1);
    mappedValueHalSensor2 = map(analogRead(halSensor2), 0, 1023, 0, 1);
    mappedValueHalSensor3 = map(analogRead(halSensor3), 0, 1023, 0, 1);
}

void HalController::doWork()
{
    readHalSensors();

  if (p_mappedValueHalSensor1 != mappedValueHalSensor1)
  {
    if (p_mappedValueHalSensor1 == 0 && p_mappedValueHalSensor2 == 0 && mappedValueHalSensor1 == 1)
    {
      Serial.println("Turning CCW");
      direction = false;
      moving = true;
    }
    p_mappedValueHalSensor1 = mappedValueHalSensor1;
    lastHalChange = millis();
  }

  if (p_mappedValueHalSensor2 != mappedValueHalSensor2)
  {
    if (p_mappedValueHalSensor1 == 0 && p_mappedValueHalSensor2 == 0 && mappedValueHalSensor2 == 1)
    {
      Serial.println("Turning CW");
      direction = true;
      moving = true;
    }
    p_mappedValueHalSensor2 = mappedValueHalSensor2;
    lastHalChange = millis();
  }

  if(lastHalChange + 500 < millis())
  {
    if(moving == true)
    {
      Serial.println("Not moving");
    }
    moving = false;
    
  }
}