#include <Arduino.h>

class HalController
{
public:
    HalController(/* args */);
    ~HalController();
    void doWork();
    void readHalSensors();


    const int halSensor1 = A0;
    const int halSensor2 = A1;
    const int halSensor3 = A2;

    int p_mappedValueHalSensor1 = 0;
    int p_mappedValueHalSensor2 = 0;
    int p_mappedValueHalSensor3 = 0;

    int mappedValueHalSensor1 = 0;
    int mappedValueHalSensor2 = 0;
    int mappedValueHalSensor3 = 0;

    uint32_t lastHalChange = millis();

    bool direction;
    bool moving = false;

private:
    /* data */
};
