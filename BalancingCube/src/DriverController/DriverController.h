#ifndef __DRIVERCONTROLLER__H
#define __DRIVERCONTROLLER__H

#include <Arduino.h>

#define MOTOR_DRIVER_PIN_ENABLE     22
#define MOTOR_DRIVER_PIN_DIRECTION  23
#define MOTOR_DRIVER_PIN_CURRENT    4
#define MOTOR_DRIVER_PIN_SPEED      A2 


class DriverController
{
public:
    DriverController(/* args */);
    ~DriverController();

    void enable();
    void disable();

    void setDirection(bool); // true forward false backward
    uint32_t getFlyWheelSpeedInRad();

    void setCurrent(uint32_t);

    void doWork();
private:
    /* data */
    uint32_t current = 0;
    bool motorDirection = false;
    bool motorEnabled = false;

    uint32_t flyWheelSpeedRPM = 0;
    uint32_t flyWheelSpeedRAD = 0;
};



#endif

