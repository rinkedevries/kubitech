#include "DriverController.h"


DriverController::DriverController(/* args */)
{
    pinMode(MOTOR_DRIVER_PIN_CURRENT, OUTPUT);    
    pinMode(MOTOR_DRIVER_PIN_DIRECTION, OUTPUT);   
    pinMode(MOTOR_DRIVER_PIN_ENABLE, OUTPUT);  

    digitalWrite(MOTOR_DRIVER_PIN_DIRECTION,motorDirection);
    digitalWrite(MOTOR_DRIVER_PIN_DIRECTION,motorEnabled);
}

DriverController::~DriverController()
{

}

void DriverController::enable()
{
    if(motorEnabled != true)
    {
        this->motorEnabled = true;
        digitalWrite(MOTOR_DRIVER_PIN_ENABLE, HIGH);
    }
    
}

void DriverController::setDirection(bool dir)
{
    if(motorDirection != dir)
    {
        this->motorDirection = dir;
        digitalWrite(MOTOR_DRIVER_PIN_DIRECTION, dir);
    }   
}

void DriverController::disable()
{
    if(motorEnabled != false)
    {
        this->motorEnabled = false;
        digitalWrite(MOTOR_DRIVER_PIN_ENABLE, LOW);
    }
}

void DriverController::doWork()
{
    uint32_t rpmFlywheel = analogRead(MOTOR_DRIVER_PIN_SPEED);
    flyWheelSpeedRPM = map(rpmFlywheel,0,238,0,2000); 

    // Convert RPM to RAD
    uint32_t flyWheelSpeedRPS = flyWheelSpeedRPM / 60;
    flyWheelSpeedRAD = flyWheelSpeedRPS * 2 * PI;

    // Serial.print("FlyWheelSpeedAmps: ");
    // Serial.print(rpmFlywheel);
    // Serial.print(" flyWheelSpeedRPM: ");
    // Serial.print(flyWheelSpeedRPM);
    // Serial.print(" flyWheelSpeedRPS: ");
    // Serial.print(flyWheelSpeedRPS);
    // Serial.print(" flyWheelSpeedRAD: ");
    // Serial.print(flyWheelSpeedRAD);


}

void DriverController::setCurrent(uint32_t value)
{
    if(current != value)
    {
        analogWrite(MOTOR_DRIVER_PIN_CURRENT,value);
        current = value;
    }
    
}

uint32_t DriverController::getFlyWheelSpeedInRad()
{
    return flyWheelSpeedRAD;
}