#include <Arduino.h>
#include "./MPUController/MpuController.h"
#include "./HalController/HalController.h"
#include "./DriverController/DriverController.h"

int starttime = 0;
int endtime = 0;
int count = 1;
int lasttimeshown = 0;

MpuController * mpuController;
HalController * halController;
DriverController * driverController;



void setup()
{
  mpuController = new MpuController();
  driverController = new DriverController();
  mpuController->registerDriverController(driverController);
  Serial.begin(115200);
  Serial.println("Begin");


  driverController->setDirection(true);
  driverController->setCurrent(100);
  driverController->enable();

  delay(5000);

  driverController->disable();

  delay(5000);

  driverController->enable();



}


void loop()
{


  mpuController->doWork();
  driverController->doWork();

  // Serial.println(mpuController->getCubeAngle());

  // Regelaar
  double Imotor = 0;
  double hoek_kubus = 0;
  double hoeksnelheid_kubus = 0;
  double snelheid_vliegwiel = 0;

  hoek_kubus = mpuController->getCubeAngle();
  hoeksnelheid_kubus = mpuController->getCubeSpeed();
  snelheid_vliegwiel = driverController->getFlyWheelSpeedInRad();

  Imotor = -40 * hoek_kubus -4* hoeksnelheid_kubus -0.1* snelheid_vliegwiel;

  if(Imotor > 0) driverController->setDirection(true);
  else if(Imotor < 0)
  {
    driverController->setDirection(false);
    Imotor = Imotor * -1;
  } 
  driverController->setCurrent(Imotor);

  Serial.print("hoek_kubus : ");
  Serial.print(hoek_kubus);
  Serial.print(" hoeksnelheid_kubus : ");
  Serial.print(hoeksnelheid_kubus);
  Serial.print(" Imotor : ");
  Serial.print(Imotor);
  Serial.print(" snelheid_vliegwiel : ");
  Serial.println(snelheid_vliegwiel);
  delay(10);


  

}
