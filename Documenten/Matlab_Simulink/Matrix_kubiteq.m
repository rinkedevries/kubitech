Km = 25.1*10^-3;   % Nm*(A^-1)
l = 0.085;         % m
lb = 0.075;        % m
mb = 0.419;        % kg
mw = 0.204;        % kg
Ib = 3.34*10^-3;   % kg*(m^2)
Iw = 0.57*10^-3;   % kg*(m^2)
Cb = 1.02*10^-3;   % kg*(m^2)*(s^-1)
Cw = 0.05*10^-3;   % kg*(m^2)*(s^-1)
g = 9.81;

A = [ 0, 1, 0; (mb*lb+mw*l)*g/(lb+mw*l^2), -(Cb/(Ib+mw*l^2)), Cw/(Ib+mw*l^2); -(mb*lb+mw*l)*g/(Ib+mw*l^2), Cb/(Ib+mw*l^2), -(Cw*(Ib+Iw+mw*l^2))/(Iw*(Ib+mw*l^2)) ];
B = [ 0; -(Km/(Ib+mw*l^2)); Km*(Ib+Iw+mw*l^2)/(Iw*(Ib+mw*l^2)) ];
C = [ 1, 0, 0; 0, 1, 0; 0, 0, 1 ];
D = 0;
sys = ss(A,B,C,D);
sysl = tf(sys);
sisotool(sysl(1));